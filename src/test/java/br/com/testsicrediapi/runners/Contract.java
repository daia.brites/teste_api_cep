package br.com.testsicrediapi.runners;

import br.com.testsicrediapi.tests.GetCepTest;
import org.junit.experimental.categories.Categories;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Categories.class)
@Categories.IncludeCategory(br.com.testsicrediapi.suites.Contract.class)
@Suite.SuiteClasses({
        GetCepTest.class
})

public class Contract {
}

