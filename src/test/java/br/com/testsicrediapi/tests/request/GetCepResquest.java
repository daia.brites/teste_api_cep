package br.com.testsicrediapi.tests.request;

import io.qameta.allure.Step;
import io.restassured.response.Response;

import static io.restassured.RestAssured.given;

public class GetCepResquest {

    @Step("Consulta CEP valido")
    public Response cepValido() {
        return given()
                .when()
                .get("92020020/json"); //https://viacep.com.br/ws/92020020/json/
    }

    @Step("Consulta CEP inexistente")
    public Response cepInexistente() {
        return given()
                .when()
                .get("92020021/json"); //https://viacep.com.br/ws/92020021/json/
    }

    @Step("Consulta CEP com formato inválido")
    public Response cepInvalido() {
        return given()
                .when()
                .get("123456789/json"); //https://viacep.com.br/ws/123456789/json/
    }

    @Step("Consulta Lista de CEPs validos por Estado, Cidade e Logradouro")
    public Response cepsFiltroPorEstadoCidadeLogradouro() {
        return given()
                .when()
                .get("RS/Gravatai/Barroso/json"); //https://viacep.com.br/ws/RS/Gravatai/Barroso/json/
    }

    @Step("Consulta Lista de CEPs por Estado Inválido, Cidade e Logradouro")
    public Response cepsFiltroPorEstadoInvalidoCidadeLogradouro() {
        return given()
                .when()
                .get("12/Gravatai/Barroso/json"); //https://viacep.com.br/ws/12/Gravatai/Barroso/json/
    }
}
