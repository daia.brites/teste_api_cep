package br.com.testsicrediapi.tests;

import br.com.testsicrediapi.suites.Contract;
import br.com.testsicrediapi.tests.request.GetCepResquest;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.junit4.DisplayName;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import java.io.File;

import static io.restassured.module.jsv.JsonSchemaValidator.matchesJsonSchema;

public class GetCepTest extends BaseTest {

    GetCepResquest getCepResquest = new GetCepResquest();

    @Test
    @Severity(SeverityLevel.BLOCKER)
    @Category(Contract.class)
    @DisplayName("Consulta de CEP válido")
    public void validarCepExistente() throws Exception {
        getCepResquest.cepValido().then()
                .statusCode(200)
                .assertThat()
                .body(
                        matchesJsonSchema(
                                new File("src/test/java/br/com/testsicrediapi/tests/contracts/cepOk.json")
                        )
                );
    }

    @Test
    @Severity(SeverityLevel.CRITICAL)
    @Category(Contract.class)
    @DisplayName("Consulta de CEP Inexistente")
    public void validarCepInexistente() throws Exception {
        getCepResquest.cepInexistente().then()
                .statusCode(200)
                .assertThat()
                .body(
                        matchesJsonSchema(
                                new File("src/test/java/br/com/testsicrediapi/tests/contracts/cepNotFound.json")
                        )
                );
    }

    @Test
    @Severity(SeverityLevel.CRITICAL)
    @Category(Contract.class)
    @DisplayName("Consulta de CEP Inválido")
    public void validarCepInvalido() throws Exception {
        getCepResquest.cepInvalido().then()
                .statusCode(400);
    }

    @Test
    @Severity(SeverityLevel.BLOCKER)
    @Category(Contract.class)
    @DisplayName("Consulta de CEPs válidos por Estado, Cidade e Logradouro")
    public void validarCepsOkFiltroPorEstadoCidadeLogradouro() throws Exception {
        getCepResquest.cepsFiltroPorEstadoCidadeLogradouro().then()
                .statusCode(200)
                .assertThat()
                .body(
                        matchesJsonSchema(
                                new File("src/test/java/br/com/testsicrediapi/tests/contracts/cepListOk.json")
                        )
                );
    }

    @Test
    @Severity(SeverityLevel.BLOCKER)
    @Category(Contract.class)
    @DisplayName("Consulta de CEPs por Estado, Cidade e Logradouro. Estado inválido")
    public void validarCepsFiltroPorEstadoInvalidoCidadeLogradouro() throws Exception {
        getCepResquest.cepsFiltroPorEstadoInvalidoCidadeLogradouro().then()
                .statusCode(400);

    }

}
