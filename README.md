# Teste automatizado de API 

## 🎯 Preparação 
Primeiramente foram realizados testes funcionais no Postman, para identificar os retornos das chamadas e obter entendimento sobre o que seria uma busta com CEP valido, inexistente, inválido e a busca da lista de CEPs através do Estado, Cidade e Logradouro.

A técnica utilizada foi teste de contrato, pois é baseado na saída gerada após a requisição, que retorna os dados em forma de arquivos (json, xml, yaml, etc).

O cenário "Consulta de CEP Inválido" não testa o body de retorno em formato json, pois retorna uma página html com erro 400 (fora do padrão REST).

Neste caso, foram realizados testes comparando: url destino, dados de retorno em formado json e código de status HTTP do retorno. 


Para que os testes sejam realizados, foram utilizadas as seguintes ferramentas:

- Restassured
- JAVA
- Maven
- Junit
- Allure Report
- IntelliJ



### Instruções para executar a automação de testes de API com Allure Report
Para gerar os relatórios de testes do Allure, é necessário fazer a instalação do Allure conforme a documentação oficial: https://docs.qameta.io/allure/#_installing_a_commandline

### 📈 Relatórios dos testes
Relatório do Allure Report com os 4 cenários e um cenário extra de erro na busca por CEPs ao informar UF em formato inválido.
![allure_report.png](allure_report.png)
